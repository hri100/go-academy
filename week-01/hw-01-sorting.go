package main

import (
	"fmt"
	"sort"
)

func sortPlanetsByRadius() {
	planets := []struct {
		Name   string
		Radius int
	}{
		{"Earth", 6371},
		{"Venus", 6051},
		{"Mars", 3389},
		{"Jupiter", 69911},
		{"Saturn", 58232},
	}

	sort.SliceStable(planets, func(i, j int) bool {
		return planets[i].Radius < planets[j].Radius
	})

	fmt.Println(planets)
}

func sortNumbers() {
	var numbers = []int{122, 234, 4312, 1235, 1234, 1236}
	mergeSort(numbers)
	fmt.Println(numbers)
}

func mergeSort(numbers []int) {
	var helper = make([]int, len(numbers))
	mergeSortHelper(numbers, helper, 0, len(numbers)-1)
}

func mergeSortHelper(numbers []int, helper []int, low int, high int) {
	if low < high {
		mid := low + (high-low)/2
		mergeSortHelper(numbers, helper, low, mid)
		mergeSortHelper(numbers, helper, mid+1, high)
		merge(numbers, helper, low, mid, high)
	}
}

func merge(numbers []int, helper []int, low, mid, high int) {
	for i := low; i <= high; i++ {
		helper[i] = numbers[i]
	}

	helperLeft := low
	helperRight := mid + 1
	current := low

	for helperLeft <= mid && helperRight <= high {
		if helper[helperLeft] <= helper[helperRight] {
			numbers[current] = helper[helperLeft]
			helperLeft++
		} else {
			numbers[current] = helper[helperRight]
			helperRight++
		}
		current++
	}

	remaining := mid - helperLeft

	for i := 0; i <= remaining; i++ {
		numbers[current+i] = helper[helperLeft+i]
	}
}

func main() {
	sortNumbers()
	sortPlanetsByRadius()
}
